---
openapi: 3.0.3
info:
  title: "Ukheshe Technologies :  ReST API"
  description:
    "This is a a ReSTful web service using JSON as the data transfer model.\
    \ The service covers all the functionality used by the uKheshe App as well as\
    \ some intended for external parties to integrate into uKheshe for Payroll and\
    \ eCommerce payments. The Integration Guide with general guidelines and use cases\
    \ can be found here: https://docs.google.com/document/d/1j_5wGytWFp6KP6bTy_nxwdE0lCwW0Lwhfe7lDWQOaus/edit?usp=sharing"
  version: 3.0.0
tags:
  - name: Telkom
paths:
  /config/{configName}:
    get:
      tags:
        - Telkom
      summary: Get the value of a requested config of Telkom tenant from Eclipse
      description:
        Returns the value based on the configName which must start with
        public.
      parameters:
        - name: configName
          in: path
          required: true
          schema:
            type: string
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/Avp"
      security:
        - JWTBearerAuthorisation: []
  /customers:
    post:
      tags:
        - Telkom
      summary: Create a new customer
      description:
        "The minimum fields needed to create a customer are phone and phoneOtp.username\
        \ and password are needed if the customer wants to use the mPOS App. referredBy\
        \ is the customerId of the referrer for commissions. name, email are optional.\
        \ This API does not need a JWT to be used."
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Customer"
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Customer"
      security:
        - JWTBearerAuthorisation: []
  /customers/identities/{identity}/password-change-init:
    post:
      tags:
        - Telkom
      summary:
        Initiate a password change by sending an OTP to the users phone which
        can be used for a POST to password-change
      description:
        This API will initiate a password change OTP. The only required
        field is the identity (username). The backend will look up the associated
        phone number and SMS the OTP required to do a password change. This API does
        not need a JWT to be used.
      parameters:
        - name: identity
          in: path
          required: true
          schema:
            type: string
      responses:
        "200":
          description: OK
      security:
        - JWTBearerAuthorisation: []
  /customers/send-payment-receipt-email:
    post:
      tags:
        - Telkom
      summary: Send payment receipt email to the customer
      description: Send payment email receipt to the customer
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/PaymentReceiptEmail"
      responses:
        "200":
          description: OK
      security:
        - JWTBearerAuthorisation: []
  /customers/verifications:
    post:
      tags:
        - Telkom
      summary: Send an OTP sms to the provided Cell Phone number
      description:
        The method takes an array of strings. Typically only one is passed
        - the cell phone number to send the OTP to. Backend will do its best to accept
        any format of number e.g. 083... 2783... etc. This API does not need a JWT
        to be used.
      requestBody:
        content:
          application/json:
            schema:
              type: array
              items:
                type: string
      responses:
        "201":
          description: Created
      security:
        - JWTBearerAuthorisation: []
  /customers/verifications/attestation/request/{deviceId}:
    get:
      tags:
        - Telkom
      parameters:
        - name: deviceId
          in: path
          required: true
          schema:
            type: string
      responses:
        default: {}
      security:
        - JWTBearerAuthorisation: []
  /customers/verifications/attestation/response/{deviceId}:
    post:
      tags:
        - Telkom
      parameters:
        - name: deviceId
          in: path
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/AttestationResponse"
      responses:
        default: {}
      security:
        - JWTBearerAuthorisation: []
  /customers/verifications/check/app-version:
    post:
      tags:
        - Telkom
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/AppVersion"
      responses:
        default: {}
      security:
        - JWTBearerAuthorisation: []
  /customers/verifications/{identity}:
    post:
      tags:
        - Telkom
      summary: Check if a OTP is correct
      description:
        The path parameter (identity) should be the cell phone number used
        when the OTP was requested to be sent to. The OTP should be sent in the body.
        Response code 200 indicates its correct and it give the information of the
        customer as well if registered. 400 indicates its incorrect. This API does
        not need a JWT to be used.
      parameters:
        - name: identity
          in: path
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Code"
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Code"
      security:
        - JWTBearerAuthorisation: []
  /customers/{customerId}/contacts:
    post:
      tags:
        - Telkom
      summary: Add a new address for the customer.
      description:
        Creates a new address linked to the customer. You can give a customer
        multiple addresses.
      requestBody:
        content:
          application/json:
            schema:
              type: object
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: object
    parameters:
      - name: customerId
        in: path
        required: true
        schema:
          format: int64
          type: integer
  /customers/{customerId}/contacts/{type}:
    delete:
      tags:
        - Telkom
      summary: Delete customer existing contact by contact type
      description: ""
      parameters:
        - name: type
          in: path
          required: true
          schema:
            type: string
      responses:
        "204":
          description: No Content
    parameters:
      - name: customerId
        in: path
        required: true
        schema:
          format: int64
          type: integer
  /customers/{customerId}/identities:
    post:
      tags:
        - Telkom
      summary: Create a username/password for a customer.
      description:
        "Customers can then get a JWT with these credentials and access\
        \ their profile, wallets, cards etc. "
      requestBody:
        content:
          application/json:
            schema:
              type: object
      responses:
        "200":
          description: Identity created
          content:
            application/json:
              schema:
                type: object
    parameters:
      - name: customerId
        in: path
        required: true
        schema:
          format: int64
          type: integer
  /customers/{customerId}/identities/password-change:
    put:
      tags:
        - Telkom
      summary: Change a customers password
      description:
        The hash (OTP) must be valid and is typically provided by a call
        to password-change-init which will SMS the OTP to the customer. Identity is
        the customers username. Password is the new password to set. This API does
        not need a JWT to be used.
      requestBody:
        content:
          application/json:
            schema:
              type: object
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: object
    parameters:
      - name: customerId
        in: path
        required: true
        schema:
          format: int64
          type: integer
components:
  schemas:
    AppVersion:
      type: object
      properties:
        appVersion:
          type: string
        platform:
          type: string
    AttestationResponse:
      type: object
      properties:
        basicIntegrity:
          type: boolean
        ctsProfileMatch:
          type: boolean
        nonce:
          type: string
    Avp:
      type: object
      properties:
        att:
          type: string
        val:
          type: string
    BaseProduct:
      description: "A base product "
      type: object
      properties:
        amount:
          format: double
          type: number
        key:
          type: string
        product:
          type: string
    Code:
      type: object
      properties:
        otp:
          type: string
    Customer:
      description: "A customer represents a person using uKheshe "
      type: object
      properties:
        accountId:
          description:
            "Synonymous with card number (QRCode). Can be provided if the\
            \ customer has a card, or dont pass and one will be assigned"
          type: string
        availableBalance:
          description: Ignored during creation
          type: number
        cardType:
          format: int32
          description: digital wallet or virtual card wallet
          type: integer
        cityOfBirth:
          type: string
        countryOfBirth:
          type: string
        currentBalance:
          description: Ignored during creation
          type: number
        customerId:
          format: int64
          description:
            Unique identifier for a customer. System generated so no need
            to pass when creating a customer
          type: integer
        customerKycResult:
          $ref: "#/components/schemas/CustomerKycResult"
        dob:
          type: string
        email:
          description: Customers email address
          type: string
        firstName:
          description: Customers first name
          type: string
        isIdentitySet:
          type: boolean
        lastName:
          description: Customers last name
          type: string
        maidenName:
          description: Customers maiden name
          maxLength: 50
          minLength: 2
          type: string
        maritalStatus:
          type: string
        middleName:
          description: Customers middle name
          maxLength: 50
          minLength: 2
          type: string
        name:
          description: Customers full name
          type: string
        nationalIdentityNumber:
          description: National identity number)
          maxLength: 20
          minLength: 5
          type: string
        organisationId:
          format: int64
          description:
            Only pass if the customer is part of an organisation which
            has been specially created by uKheshe. You can only create customers in
            an organisation if the caller is in that organisation as well
          type: integer
        passportExpiry:
          description:
            Mandatory If the passport number is provided and must be in
            form of yyyyMMdd)
          maxLength: 8
          minLength: 8
          type: string
        passportNumber:
          description: National identity number)
          maxLength: 20
          minLength: 5
          type: string
        passportPlaceOfIssuance:
          description: Mandatory If the passport number is provided)
          maxLength: 20
          minLength: 3
          type: string
        password:
          description: Password to log in with
          type: string
        phone:
          description: Customers mobile phone number
          type: string
        phoneOtp:
          description:
            Must be provided unless API user has special configuration
            to allow bulk customer creation
          type: string
        preferences:
          description: Store weakly typed AVP's on the customer. Not required
          type: array
          items:
            $ref: "#/components/schemas/Avp"
        referralCode:
          description:
            Ignored during creation. This is what to use when the customer
            referres others
          type: string
        referredBy:
          description: CustomerId of who referred the customer
          type: string
        status:
          allOf:
            - $ref: "#/components/schemas/CustomerStatus"
            - description: Ignored during creation
        tenantId:
          format: int64
          description: digital wallet or virtual card wallet
          type: integer
        title:
          description: The title/salutation of the customer
          type: string
    CustomerKycResult:
      type: object
      properties:
        addressMatchesProofOfAddress:
          $ref: "#/components/schemas/RatifyItemResult"
        creditBureauCheck:
          $ref: "#/components/schemas/RatifyItemResult"
        firstNameMatchesNationalIdentity:
          $ref: "#/components/schemas/RatifyItemResult"
        firstNameMatchesPassport:
          $ref: "#/components/schemas/RatifyItemResult"
        identityNumberMatchesNationalIdentity:
          $ref: "#/components/schemas/RatifyItemResult"
        lastModified:
          format: date-time
          description: The date/time when the ratify test was last run
          type: string
        lastNameMatchesNationalIdentity:
          $ref: "#/components/schemas/RatifyItemResult"
        lastNameMatchesPassport:
          $ref: "#/components/schemas/RatifyItemResult"
        manualRatify:
          $ref: "#/components/schemas/RatifyItemResult"
        matchCheck:
          $ref: "#/components/schemas/RatifyItemResult"
        passportNumberMatchesPassport:
          $ref: "#/components/schemas/RatifyItemResult"
        sanctionsListCheck:
          $ref: "#/components/schemas/RatifyItemResult"
        selfieIsASelfie:
          $ref: "#/components/schemas/RatifyItemResult"
        selfieMatchesNationalIdentity:
          $ref: "#/components/schemas/RatifyItemResult"
        selfieMatchesPassport:
          $ref: "#/components/schemas/RatifyItemResult"
    CustomerStatus:
      type: object
      properties:
        enabled:
          type: boolean
        locked:
          type: boolean
    EmailType:
      enum:
        - EFT
        - GET_PAID
        - TRANSACTION_HISTORY
        - TRANSACTION_HISTORY_EFT
      type: string
    MoNice:
      description: "A Monice product "
      type: object
      properties:
        msisdn:
          type: string
        productList:
          type: array
          items:
            $ref: "#/components/schemas/ProductsDTO"
        responseMsg:
          type: string
        responseStatus:
          type: string
        sessionid:
          type: string
        uid:
          type: string
    MoNiceReqDTO:
      description: "A base product "
      type: object
      properties:
        key:
          type: string
        otp:
          type: string
        sessionId:
          type: string
    PaymentReceiptEmail:
      type: object
      properties:
        additionalInfo:
          type: array
          items:
            $ref: "#/components/schemas/Avp"
        description:
          type: string
        email:
          type: string
        emailType:
          $ref: "#/components/schemas/EmailType"
        organisationId:
          format: int64
          type: integer
        transactionId:
          format: int64
          type: integer
        userId:
          format: int64
          type: integer
        walletId:
          format: int64
          type: integer
    ProductsDTO:
      description: "A product "
      type: object
      properties:
        productGroup:
          type: string
        products:
          type: array
          items:
            $ref: "#/components/schemas/BaseProduct"
    RatifyItemResult:
      description:
        A result for an item in a ratify check. If checked =false then
        other fields wont be populated
      type: object
      properties:
        checked:
          description: Whether the item was checked
          type: boolean
        comment:
          description: A system generated comment if applicable
          type: string
        passed:
          description:
            "If the item was checked, then whether the check passed or\
            \ not based on pre defined binary rules. Passed=true implies a favourable\
            \ outcome. E.g. for a sanctions check passed=true means they are not on\
            \ a sanctions list"
          type: boolean
        score:
          description:
            "Provided as a quantitative score of the result e.g. the %\
            \ chance a face matches a photo, a credit bureau score etc"
          type: number
  securitySchemes:
    JWTBearerAuthorisation:
      type: http
      scheme: bearer
      bearerFormat: JWT
